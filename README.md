# Survey Quiz

## Description

An app for a quiz survey to show customers' opinion on regarding the questions placed.

## How to run the game

1. clone the app to your local

2. npm install

3. npm start

## How to use

1. The questions will be displayed on the top of the page.

2. User only need to answer by clicking or tapping the answer.

3. There's also a feature where you can move the plane by using keyboard arrows (up, down, left, and right) and clicking or tapping the screen to move the plane to the pointer.

4. You can also shoot by clicking or tapping twice and press space bar.

5. If you shoot the planet five times, the planet will be destroyed.