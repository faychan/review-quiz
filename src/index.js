// Import Application class that is the main part of our PIXI project
import { Application } from '@pixi/app'

// In order that PIXI could render things we need to register appropriate plugins
import { Renderer } from '@pixi/core' // Renderer is the class that is going to register plugins

import { BatchRenderer } from '@pixi/core' // BatchRenderer is the "plugin" for drawing sprites
Renderer.registerPlugin('batch', BatchRenderer)

// And just for convenience let's register Loader plugin in order to use it right from Application instance like app.loader.add(..) etc.
import { AppLoaderPlugin } from '@pixi/loaders'
Application.registerPlugin(AppLoaderPlugin)

// Sprite is our image on the stage
import { Sprite } from '@pixi/sprite'

// import "pixi-layers"; //or 
import * as PIXI from "pixi.js";
window.PIXI = PIXI;
window.PIXI[ "default" ] = PIXI;
require("pixi-layers")
var keyboard = require('pixi-keyboard');
import SOUND from "pixi-sound";
PIXI["s" + "o" + "u" + "n" + "d"] = SOUND;

// App with width and height of the page
const app = new Application({
	width: window.innerWidth,
	height: window.innerHeight,
	transparent: true,
	resizeTo: window
})
document.body.appendChild(app.view) // Create Canvas tag in the body

var levelNumber = 1, question, answerPosition, click = 0, bullet, bullets = [], thanksNote,
		answers = [], messageLeft, messageRight, messageRight2, plane1, bulletSpeed = 5,
		planets = [], planet1, planet2, planet3, percentageBar, innerBar, outerBar, closeButton,
		percentages = [25, 50, 25], percentageBars = [], percentage, planetHit = [0, 0, 0],
		percentageColor = [], recreatePercentage = false, state, planetDestroyed = [false,false,false],
		locationClicked;

app.loader
	.add('plane_1',"./assets/plane_1.png")
	.add('plane_2',"./assets/plane_2.png")
	.add('planet_1',"./assets/planet_1.png")
	.add('planet_2',"./assets/planet_2.png")
	.add('planet_3',"./assets/planet_4.png")
	.add('closeButton',"./assets/closeButton.png")
	.add('logo',"./assets/Logo_Blk.png")
	.add('bulletImage',"./assets/bullet.png")
app.loader.load(() => {
	//Display Text
	var style = new PIXI.TextStyle({
		fontFamily: "Roboto",
		fontSize: 30,
		fill: "#fff",
		stroke: '#000000',
		strokeThickness: 4,
	});

	question = new PIXI.Text('Place your question here', style);
	question.anchor.set(0.5);
	question.x = app.renderer.screen.width/2;
	question.y = 35;
	question.style = {fill: '#000', strokeThickness: 2, fontSize: 40};
	
	messageLeft = new PIXI.Text("Level : "+levelNumber, style);
	messageLeft.anchor.set(1);
	messageLeft.x = messageLeft.width + 45;
	messageLeft.y = app.renderer.screen.height - 20;
	
	messageRight = new PIXI.Text("Power By :", style);
	messageRight2 = new PIXI.Sprite.from('logo')
	messageRight.style = {fontSize: 20, strokeThickness: 0};
	messageRight.anchor.set(0.5, 1);
	messageRight.x = app.renderer.screen.width - messageRight.width - messageRight2.width;
	messageRight.y = app.renderer.screen.height - 25;
	
	messageRight2.anchor.set(0.5, 1);
	messageRight2.x = app.renderer.screen.width - messageRight2.width + 20;
	messageRight2.y = app.renderer.screen.height - 20;
	messageRight2.scale.x = 0.5;
	messageRight2.scale.y = 0.5;
	
	app.stage.addChild(question);	
	app.stage.addChild(messageLeft);
	app.stage.addChild(messageRight);
	app.stage.addChild(messageRight2);

	//closeButton
	closeButton = new PIXI.Sprite.from('closeButton')
	closeButton.anchor.set(0.5);
	closeButton.scale.x = 0.5;
	closeButton.scale.y = 0.5;
	closeButton.x = app.renderer.screen.width - closeButton.width + 10;
	closeButton.y = closeButton.height - 10;
	closeButton.interactive = true;
	closeButton.pointertap = closeCanvas;
	app.stage.addChild(closeButton);	
	
	//show plane
	plane1 = new PIXI.Sprite.from('plane_1')

	//show planets
	planet1 = new PIXI.Sprite.from('planet_1')
	planet1.x = app.renderer.screen.width / 2;
	planet1.y = 100;
	planet1.scale.x = 0.4;
	planet1.scale.y = 0.4;
	planet2 = new PIXI.Sprite.from('planet_2')
	planet2.x = app.renderer.screen.width / 2;
	planet2.y = app.renderer.screen.height/2 - 50;
	planet2.scale.x = 0.4;
	planet2.scale.y = 0.4;
	planet3 = new PIXI.Sprite.from('planet_3')
	planet3.x = app.renderer.screen.width / 2;
	planet3.y = app.renderer.screen.height - 200;
	planet3.scale.x = 0.4;
	planet3.scale.y = 0.4;
	planets.push(planet1, planet2, planet3)

	app.stage.addChild(planet1, planet2,planet3);

	//answer style
	var style2 = new PIXI.TextStyle({
		fontFamily: "Roboto",
		fontSize: 30,
		fill: "#00000",
	});
	//show answers
	let textAnswer = ['Answer1', 'Answer2', 'Answer3']
	answerPosition = [
		planet1.x + planet1.width + 50, 
		planet1.y + 20, 
		planet2.x + planet2.width + 50, 
		planet2.y + 20,
		planet3.x + planet3.width + 50,
		planet3.y + 20
	]

	for (var i = 0; i < 3; i++){
    var answer = new PIXI.Text(textAnswer[i], style2);
    answer.buttonMode = true;

    answer.x = answerPosition[i*2];
    answer.y = answerPosition[i*2+1];

    // make the answer interactive...
		// answer.interactive = true;
        
		// answer.pointertap = showPercentage;
		answers.push(answer);
    // add it to the stage
		app.stage.addChild(answer);
	}

	PIXI.sound.add({
		bulletSound: './assets/bulletSound.wav',
		destroyedSound:  './assets/destroyedSound.wav',
	});
	
	responsive()
	showPlane()
})

function closeCanvas() {
	for (var i = app.stage.children.length - 1; i >= 0; i--) {
		app.stage.removeChild(app.stage.children[i]);
	};
}

function hitTestRectangle(r1, r2) {

  //Define the variables we'll need to calculate
  let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

  //hit will determine whether there's a collision
  hit = false;

  //Find the center points of each sprite
  r1.centerX = r1.x + r1.width / 2;
  r1.centerY = r1.y + r1.height / 2;
  r2.centerX = r2.x + r2.width / 2;
  r2.centerY = r2.y + r2.height / 2;

  //Find the half-widths and half-heights of each sprite
  r1.halfWidth = r1.width / 2;
  r1.halfHeight = r1.height / 2;
  r2.halfWidth = r2.width / 2;
  r2.halfHeight = r2.height / 2;

  //Calculate the distance vector between the sprites
  vx = r1.centerX - r2.centerX;
  vy = r1.centerY - r2.centerY;

  //Figure out the combined half-widths and half-heights
  combinedHalfWidths = r1.halfWidth + r2.halfWidth;
  combinedHalfHeights = r1.halfHeight + r2.halfHeight;

  //Check for a collision on the x axis
  if (Math.abs(vx) < combinedHalfWidths) {

    //A collision might be occurring. Check for a collision on the y axis
    if (Math.abs(vy) < combinedHalfHeights) {

      //There's definitely a collision happening
      hit = true;
    } else {

      //There's no collision on the y axis
      hit = false;
    }
  } else {

    //There's no collision on the x axis
    hit = false;
  }

  //`hit` will be either `true` or `false`
  return hit;
};

app.renderer.plugins.interaction.on('pointerup', movePlane);

function movePlane (event) {
	click++;
	if(click > 1 && (event.data.global.x == locationClicked || (event.data.global.x - locationClicked <= 5 && event.data.global.x - locationClicked > 0) || (locationClicked - event.data.global.x <= 5 && locationClicked - event.data.global.x > 0))){
		bullet = new PIXI.Sprite.from('bulletImage')
		if(app.renderer.screen.width < 768){
			bullet.scale.x = 0.3;
			bullet.scale.y = 0.3;
			bullet.position.x = plane1.x + plane1.width - 30;
			bullet.position.y = plane1.y - 5;
		} else {
			bullet.scale.x = 0.5;
			bullet.scale.y = 0.5;
			bullet.position.x = plane1.x + plane1.width - 40;
			bullet.position.y = plane1.y - 15;
		}
		PIXI.sound.play('bulletSound');;
		app.stage.addChild(bullet);
		bullets.push(bullet);
	} else {
		plane1.x = event.data.global.x;
		plane1.y = event.data.global.y;
	}

	locationClicked = event.data.global.x;

	if(click == 1){
		setTimeout(function(){ click = 0}, 1000);
	}
}

animateBullet();  
function animateBullet() {  
  requestAnimationFrame(animateBullet);

  for(var b=bullets.length-1;b>=0;b--){
		for(var p=planets.length-1;p>=0;p--){
			if(hitTestRectangle(bullets[b], planets[p]) && !planetDestroyed[p]){
				showPercentage(p);
				app.stage.removeChild(bullets[b])
				bullets.splice( b, 1 );
				planetHit[p] += 1;
				if(planetHit[p] == 5){
					planetDestroyed[p] = true;
					app.stage.removeChild(planets[p]);
					PIXI.sound.play('destroyedSound');
				}
			} else{
				if(app.renderer.screen.width < 768){
					bullets[b].scale.x = 0.3;
					bullets[b].scale.y = 0.3;
				} else {
					bullets[b].scale.x = 0.5;
					bullets[b].scale.y = 0.5;
				}
				bullets[b].position.x += bulletSpeed;
			}
		}
  }
  // render the container
	app.renderer.render(app.stage);
	PIXI.keyboardManager.update();
}

PIXI.keyboardManager.on('pressed', function(key){
  if(key == 32){
		bullet = new PIXI.Sprite.from('bulletImage')
		if(app.renderer.screen.width < 768){
			bullet.scale.x = 0.3;
			bullet.scale.y = 0.3;
			bullet.position.x = plane1.x + plane1.width - 30;
			bullet.position.y = plane1.y - 5;
		} else {
			bullet.scale.x = 0.5;
			bullet.scale.y = 0.5;
			bullet.position.x = plane1.x + plane1.width - 40;
			bullet.position.y = plane1.y - 15;
		}
		PIXI.sound.play('bulletSound');;
		app.stage.addChild(bullet);
		bullets.push(bullet);
	}
});

var showPercentage = function (planet) {
	var style2 = new PIXI.TextStyle({
		fontFamily: "Roboto",
		fontSize: 30,
		fill: "#00000",
	});
	
	if(percentageBars.length == 0){
		thanksNote = new PIXI.Text('Thank you for your feedback!', style2);	
		if(app.renderer.screen.width < 768){
			thanksNote.style = {fontSize: 16};
			thanksNote.x = app.renderer.screen.width/2;
			thanksNote.y = question.y + 25;
		} else {
			thanksNote.x = app.renderer.screen.width/2;
			thanksNote.y = question.y + 40;
		}
		thanksNote.anchor.set(0.5);
		app.stage.addChild(thanksNote);

		//Create the percentage bar
		for (var j = 0; j < 3; j++){
			percentageBar = new PIXI.Container();
			app.stage.addChild(percentageBar);
			
			//Create the grey transparent background rectangle
			innerBar = new PIXI.Graphics();
			innerBar.beginFill(0x000000, 0.1);
			
			percentage = new PIXI.Text(percentages[j]+'%', style2);	
			//Create the colored rectangle
			outerBar = new PIXI.Graphics();
			if(j == planet){
				outerBar.beginFill(0x7abd9e, 0.5);
				percentageColor.push(0x7abd9e)
			} else {
				outerBar.beginFill(0xa0a18f, 0.5);
				percentageColor.push(0xa0a18f)
			}
			
			if(app.renderer.screen.width < 768){
				percentage.style = {fontSize : 15}
				percentageBar.position.set(answerPosition[j*2], answerPosition[j*2+1] + 20)
				
				innerBar.drawRoundedRect(0, 0, planet1.width, answers[0].height + 14, 10);

				percentage.x = innerBar.x + innerBar.width - percentage.width - 5;
				percentage.y = innerBar.y + 5;

				outerBar.drawRoundedRect(0, 0, innerBar.width * percentages[j]/100, answers[0].height + 14, 10);
			} else {
				percentageBar.position.set(answerPosition[j*2] - 30, answerPosition[j*2+1] - 8)
				percentage.style = {fontSize : 35}
				
				innerBar.drawRoundedRect(0, 0, (app.renderer.screen.width/2) - planet1.width - 70, answers[0].height + 20, 10);

				percentage.x = innerBar.x + innerBar.width - 80;
				percentage.y = innerBar.y + 8;

				outerBar.drawRoundedRect(0, 0, innerBar.width * percentages[j]/100, answers[0].height + 20, 10);
			}
			
			innerBar.endFill();
			
			outerBar.endFill();

			percentageBar.addChild(innerBar);
			percentageBar.addChild(percentage);	

			percentageBar.addChild(outerBar);
			percentageBar.outer = outerBar;

			percentageBars.push(percentageBar);
		}
	}
};

function showPlane(){
	// set the anchor point so the texture is centerd on the sprite
	plane1.anchor.set(0.5);

	// finally lets set the plane1 to be at a random position..
	plane1.x = app.renderer.screen.width/8;
	plane1.y = app.screen.height/4;

	plane1.vx = 0;
	plane1.vy = 0;

	// create a random speed for the plane1 between 0 - 2
	plane1.speed = 5;
	app.stage.addChild(plane1);

	let left = keyboardArrows("ArrowLeft"),
      up = keyboardArrows("ArrowUp"),
      right = keyboardArrows("ArrowRight"),
			down = keyboardArrows("ArrowDown");
			
  //Left arrow key `press` method
  left.press = () => {
    //Change the plane1's velocity when the key is pressed
    plane1.vx = -5;
    plane1.vy = 0;
  };
  
  //Left arrow key `release` method
  left.release = () => {
    //If the left arrow has been released, and the right arrow isn't down,
    //and the plane1 isn't moving vertically:
    //Stop the plane1
    if (!right.isDown && plane1.vy === 0) {
      plane1.vx = 0;
    }
  };

  //Up
  up.press = () => {
    plane1.vy = -5;
    plane1.vx = 0;
  };
  up.release = () => {
    if (!down.isDown && plane1.vx === 0) {
      plane1.vy = 0;
    }
  };

  //Right
  right.press = () => {
    plane1.vx = 5;
    plane1.vy = 0;
  };
  right.release = () => {
    if (!left.isDown && plane1.vy === 0) {
      plane1.vx = 0;
    }
  };

  //Down
  down.press = () => {
		plane1.vy = 5;
		plane1.vx = 0;
  };
  down.release = () => {
    if (!up.isDown && plane1.vx === 0) {
      plane1.vy = 0;
    }
  };

  //Set the game state
  state = play;
 
  //Start the game loop 
  app.ticker.add(delta => gameLoop(delta));
}

function keyboardArrows(value) {
  let key = {};
  key.value = value;
  key.isDown = false;
  key.isUp = true;
  key.press = undefined;
  key.release = undefined;
  //The `downHandler`
  key.downHandler = event => {
    if (event.key === key.value) {
      if (key.isUp && key.press) key.press();
      key.isDown = true;
      key.isUp = false;
      event.preventDefault();
    }
  };

  //The `upHandler`
  key.upHandler = event => {
    if (event.key === key.value) {
      if (key.isDown && key.release) key.release();
      key.isDown = false;
      key.isUp = true;
      event.preventDefault();
    }
  };

  //Attach event listeners
  const downListener = key.downHandler.bind(key);
  const upListener = key.upHandler.bind(key);
  
  window.addEventListener(
    "keydown", downListener, false
  );
  window.addEventListener(
    "keyup", upListener, false
  );
  
  // Detach event listeners
  key.unsubscribe = () => {
    window.removeEventListener("keydown", downListener);
    window.removeEventListener("keyup", upListener);
  };
  
  return key;
}

function gameLoop(delta){
  //Update the current game state:
  state(delta);
}

function play(delta) {
  //Use the plane1's velocity to make it move
  plane1.x += plane1.vx;
  plane1.y += plane1.vy
}

function responsive(){
	var w = app.renderer.screen.width;

	if(w < 768){
		question.style = {fontSize: 20};
		question.x = app.renderer.screen.width/2;
		question.y = 25;
		if(thanksNote != null){
			thanksNote.style = {fontSize: 16};
			thanksNote.x = app.renderer.screen.width/2 - thanksNote.width;
			thanksNote.y = question.y + 20;
		}
		messageLeft.style = {fontSize: 18};
		messageLeft.x = messageLeft.width + 15;
		messageLeft.y = app.renderer.screen.height - 20;
		messageRight.style = {fontSize: 12};
		messageRight2.scale.x = 0.3;
		messageRight2.scale.y = 0.3;
		messageRight.x = app.renderer.screen.width - messageRight2.width - 45;
		messageRight.y = app.renderer.screen.height - 20;
		messageRight2.x = app.renderer.screen.width - messageRight2.width + 25;
		messageRight2.y = app.renderer.screen.height - 18;

		plane1.scale.x = 0.5;
		plane1.scale.y = 0.5;
		if(plane1.x > w){
			plane1.x = w - plane1.width;
		}
		if(plane1.y > app.renderer.screen.height){
			plane1.y = app.renderer.screen.height - plane1.height;
		}
		
		planet1.scale.x = 0.3;
		planet1.scale.y = 0.3;
		planet2.scale.x = 0.3;
		planet2.scale.y = 0.3;
		planet3.scale.x = 0.3;
		planet3.scale.y = 0.3;

		planet1.x = app.renderer.screen.width - planet1.width - 20;
		planet1.y = 70;
		planet2.x = app.renderer.screen.width - planet2.width - 20;
		planet2.y = app.renderer.screen.height/2 - 50;
		planet3.x = app.renderer.screen.width - planet3.width - 20;
		planet3.y = app.renderer.screen.height - 170;

		closeButton.scale.x = 0.3;
		closeButton.scale.y = 0.3;
		closeButton.x = app.renderer.screen.width - closeButton.width + 10;
		closeButton.y = closeButton.height - 10;

		answerPosition = [
			planet1.x, 
			planet1.y + planet1.height + 10, 
			planet2.x, 
			planet2.y + planet2.height + 10,
			planet3.x,
			planet3.y + planet3.height + 10
		]

		var style2 = new PIXI.TextStyle({
			fontFamily: "Roboto",
			fontSize: 15,
			fill: "#00000",
		});

		if(percentageBars.length > 0){
			percentageBars[0].removeChild(percentageBars[0].children[0], percentageBars[0].children[1], percentageBars[0].children[2]);
			percentageBars[1].removeChild(percentageBars[1].children[0], percentageBars[1].children[1], percentageBars[1].children[2]);
			percentageBars[2].removeChild(percentageBars[2].children[0], percentageBars[2].children[1], percentageBars[2].children[2]);
			percentageBars = [];

			recreatePercentage = true;
		}

		for (var i = 0; i < 3; i++){
			answers[i].x = answerPosition[i*2];
			answers[i].y = answerPosition[i*2+1];
			answers[i].style = {fontSize: 13}
			if(recreatePercentage){
				percentageBar = new PIXI.Container();
				percentageBar.position.set(answerPosition[i*2], answerPosition[i*2+1] + 20)
				app.stage.addChild(percentageBar);

				innerBar = new PIXI.Graphics();
				innerBar.beginFill(0x000000, 0.1);
				innerBar.drawRoundedRect(0, 0, planet1.width, answers[i].height + 10, 10);
				innerBar.endFill();
				
				percentage = new PIXI.Text(percentages[i]+'%', style2);
				percentage.x = innerBar.x + innerBar.width - percentage.width - 5;
				percentage.y = innerBar.y + 5;

				percentageBar.addChild(innerBar);
				percentageBar.addChild(percentage);		

				//Create the colored rectangle
				outerBar = new PIXI.Graphics();
				outerBar.beginFill(percentageColor[i], 0.5);
				outerBar.drawRoundedRect(0, 0, innerBar.width * percentages[i]/100, answers[i].height + 10, 10);
				outerBar.endFill();

				percentageBar.addChild(outerBar);

				percentageBar.outer = outerBar;

				percentageBars.push(percentageBar);
			}
		}
	} else if(w >= 768){
		question.style = {fontSize: 40};
		question.x = app.renderer.screen.width/2;
		question.y = 35;
		if(thanksNote != null){
			thanksNote.style = {fontSize: 30};
			thanksNote.x = app.renderer.screen.width/2 - thanksNote.width;
			thanksNote.y = question.y + 20;
		}
		messageLeft.style = {fontSize: 30};
		messageLeft.x = messageLeft.width + 45;
		messageLeft.y = app.renderer.screen.height - 20;
		messageRight.style = {fontSize: 20};
		messageRight2.scale.x = 0.5;
		messageRight2.scale.y = 0.5;
		messageRight.x = app.renderer.screen.width - messageRight.width - messageRight2.width;
		messageRight.y = app.renderer.screen.height - 25;;
		messageRight2.x = app.renderer.screen.width - messageRight2.width + 20;
		messageRight2.y = app.renderer.screen.height - 20;

		plane1.scale.x = 1;
		plane1.scale.y = 1;
		if(plane1.x > w){
			plane1.x = w - plane1.width;
		}
		if(plane1.y > app.renderer.screen.height){
			plane1.y = app.renderer.screen.height - plane1.height;
		}

		planet1.x = app.renderer.screen.width / 2;
		planet1.y = 100;
		planet2.x = app.renderer.screen.width / 2;
		planet2.y = app.renderer.screen.height/2 - 50;
		planet3.x = app.renderer.screen.width / 2;
		planet3.y = app.renderer.screen.height - 200;
		planet1.scale.x = 0.4;
		planet1.scale.y = 0.4;
		planet2.scale.x = 0.4;
		planet2.scale.y = 0.4;
		planet3.scale.x = 0.4;
		planet3.scale.y = 0.4;

		closeButton.scale.x = 0.5;
		closeButton.scale.y = 0.5;
		closeButton.x = app.renderer.screen.width - closeButton.width + 10;
		closeButton.y = closeButton.height - 10;

		answerPosition = [
			planet1.x + planet1.width + 50, 
			planet1.y + 20, 
			planet2.x + planet2.width + 50, 
			planet2.y + 20,
			planet3.x + planet3.width + 50,
			planet3.y + 20
		]

		var style2 = new PIXI.TextStyle({
			fontFamily: "Roboto",
			fontSize: 30,
			fill: "#00000",
		});

		for (var i = 0; i < 3; i++){
			answers[i].x = answerPosition[i*2];
			answers[i].y = answerPosition[i*2+1];
			answers[i].style = {fontSize: 30}
			if(percentageBar != null){
				percentageBars[i].position.set(answerPosition[i*2] - 30, answerPosition[i*2+1] - 8)
				percentageBars[i].removeChild(percentageBars[i].children[0], percentageBars[i].children[1], percentageBars[i].children[2]);
				innerBar = new PIXI.Graphics();
				innerBar.beginFill(0x000000, 0.1);
				innerBar.drawRoundedRect(0, 0, (app.renderer.screen.width / 2) - planet1.width*2, 50, 10);
				innerBar.endFill();
				percentage = new PIXI.Text(percentages[i]+'%', style2);
				percentage.x = innerBar.x + innerBar.width - 80;
				percentage.y = innerBar.y + 5;
				percentageBars[i].addChild(innerBar);
				percentageBars[i].addChild(percentage);		
				//Create the colored rectangle
				outerBar = new PIXI.Graphics();
				outerBar.beginFill(percentageColor[i], 0.5);
				outerBar.drawRoundedRect(0, 0, innerBar.width * percentages[i]/100, 50, 10);
				outerBar.endFill();
				percentageBars[i].addChild(outerBar);

				percentageBars[i].outer = outerBar;
			}
		}
	}
}

function resize(){
	responsive()
}

window.onresize = resize;